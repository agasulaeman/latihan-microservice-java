package com.b2camp.gangzar.usermanagement.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class RoleResponse {

    private String userUuid;
    private String roleName;

}
