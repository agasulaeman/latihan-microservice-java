package com.b2camp.gangzar.usermanagement.repository;

import com.b2camp.gangzar.usermanagement.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Integer> {
}
