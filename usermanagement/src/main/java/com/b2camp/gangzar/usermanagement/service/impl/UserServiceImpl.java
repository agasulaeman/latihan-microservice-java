package com.b2camp.gangzar.usermanagement.service.impl;

import com.b2camp.gangzar.usermanagement.dto.UserRequest;
import com.b2camp.gangzar.usermanagement.dto.UserResponse;
import com.b2camp.gangzar.usermanagement.model.Role;
import com.b2camp.gangzar.usermanagement.model.User;
import com.b2camp.gangzar.usermanagement.repository.UserRepository;
import com.b2camp.gangzar.usermanagement.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    private RoleServiceImpl roleServiceImpl;


    @Override
    public String create(User user) {
        User userSave = userRepository.save(user);
        log.info("role save {} {}", userSave, "berhasil");
        Role role = roleServiceImpl.save(userSave, userSave.getUserId());
        return null;
    }

    @Transactional
    @Override
    public UserResponse saveData(UserRequest request) {
        User userSave = new User();
        userSave.setUserId(request.getUserId());
        userSave.setUserName(request.getUserName());
        userSave.setEntryDate(request.getEntryDate());
        userSave.setRoleId(request.getRoleId());

        User savedUser = userRepository.save(userSave);

        UserResponse userResponse = new UserResponse();
        userResponse.setUserId(savedUser.getUserId());
        userResponse.setUserName(savedUser.getUserName());
        userResponse.setEntryDate(savedUser.getEntryDate());
        userResponse.setRoleId(savedUser.getRoleId());

        return userResponse;
    }

    @Override
    public Optional<User> findByRoleId(Integer roleId) {
        return Optional.empty();
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    @Override
    public List<User> findAll() {
        var userSave = userRepository.findAll();
        log.info("user save {}", userSave);
        return (List<User>) userSave;
    }

    @Override
    public void delete(Integer roleId) {
            userRepository.deleteById(roleId);
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    @Override
    public User updateData(Integer roleId,UserRequest userRequest) {
        var userFind = userRepository.findById(roleId)
                .stream()
                .peek(user -> user
                        .setRoleId(user.getRoleId() + roleId)).findFirst().orElseThrow();
        User userSave = userRepository.save(userFind);
        System.out.println(userSave);
        return userSave;
    }
}
