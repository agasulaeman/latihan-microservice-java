package com.b2camp.gangzar.usermanagement.service.impl;

import com.b2camp.gangzar.usermanagement.model.Role;
import com.b2camp.gangzar.usermanagement.model.User;
import com.b2camp.gangzar.usermanagement.repository.RoleRepository;
import com.b2camp.gangzar.usermanagement.service.RoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public Role save(User user, String roleName) {
        Role roleBuild = Role.builder().roleName(roleName).user(user).build();
        Role saveRole = roleRepository.save(roleBuild);
        return saveRole;
    }
}
