package com.b2camp.gangzar.usermanagement.exception;

public class UserException extends Exception{

    public UserException() {
        super();
    }

    public UserException(String message) {
        super(message);
    }
}
