package com.b2camp.gangzar.usermanagement.controller;

import com.b2camp.gangzar.usermanagement.dto.UserRequest;
import com.b2camp.gangzar.usermanagement.dto.UserResponse;
import com.b2camp.gangzar.usermanagement.exception.UserException;
import com.b2camp.gangzar.usermanagement.model.User;
import com.b2camp.gangzar.usermanagement.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/user")
@AllArgsConstructor
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public UserResponse add (@RequestBody UserRequest userRequest) throws UserException{
        return userService.saveData(userRequest);
    }
    @PutMapping(value = "{id}")
    public User updateUserRoleId(@PathVariable(value = "id") Integer id, @RequestBody UserRequest userRequest) throws UserException{
        return userService.updateData(id,userRequest);
    }

    @DeleteMapping(value = "{id}")
    public String deleteUserRoleId(@PathVariable(value = "id") Integer roleId) throws Exception{
        userService.delete(roleId);
        return "delete berhasil";
    }

    @GetMapping
    public List<User> listUser(){
        return userService.findAll();
    }

}
