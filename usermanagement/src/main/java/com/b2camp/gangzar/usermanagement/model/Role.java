package com.b2camp.gangzar.usermanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "master_role")
@Builder
public class Role {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String userUuid;

    @Column(name = "role_name", nullable = false)
    private String roleName;

    @Column(name = "role_number", nullable = false)
    private String role_number;

    @ManyToOne
    @JoinColumn(name = "id_role", referencedColumnName = "role_id")
    private User user;
}
