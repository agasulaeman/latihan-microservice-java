package com.b2camp.gangzar.usermanagement.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Getter
@Setter
@Table(name = "master_user")

public class User {
    @Id
    @Column(name = "user_id",nullable = false,length = 50)
    private String userId;

    @Column(name = "user_name",nullable = false,length = 100)
    private String userName;

    @Column(name = "entry_date",nullable = false,length = 50)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date entryDate;

    @Column(name = "role_id",nullable = false,length = 50)
    private int roleId;

}
