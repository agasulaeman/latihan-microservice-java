package com.b2camp.gangzar.usermanagement.service;

import com.b2camp.gangzar.usermanagement.dto.UserRequest;
import com.b2camp.gangzar.usermanagement.dto.UserResponse;
import com.b2camp.gangzar.usermanagement.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    public String create (User user);
    UserResponse saveData (UserRequest request);
    public Optional<User>findByRoleId (Integer roleId);
    List<User> findAll();
    void delete(Integer roleId);
    public User updateData(Integer roleId,UserRequest userRequest);

}
