package com.b2camp.gangzar.usermanagement.repository;

import com.b2camp.gangzar.usermanagement.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role,Integer> {

}
