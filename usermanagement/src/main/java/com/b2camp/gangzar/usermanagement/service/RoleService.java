package com.b2camp.gangzar.usermanagement.service;

import com.b2camp.gangzar.usermanagement.model.Role;
import com.b2camp.gangzar.usermanagement.model.User;

public interface RoleService {
    public Role save(User user,String roleName);
}
